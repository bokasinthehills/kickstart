## Boxes in the Clouds

![Naima Village](images/naima.jpg)

---slide---

# The Opportunity
We can leverage cheap technology and excellent educational content to empower Nepali Villagers.

---slide---
# The Vision
An organization led by a Working Group closely working with a co-ordination team in Nepal and donors across the globe to empower content creators and technical experts to bring opportunities and education to rural Nepal.

---slide---
# What do we need?

- skeptics (Roast Us!)
- friends (help us!)
- money (fund us!)
- experts (link us!)

---vertical---
<p class="stretch"><img src="images/discussion_2.png"></p>

---vertical---

<p class="stretch"><img src="images/discussion_4.png"></p>

---vertical---

<p class="stretch"><img src="images/discussion_3.png"></p>

---vertical---

# Must Haves

- A Working Group (immediate call for volunteers)
- Coordinators in Nepal
- Data

---slide---

# Interested?

- I'll scroll vertically
- else, I'll scroll horizontally
  - I'll also skip the vertical slides if I don't have enough time

---vertical---

# Collaborating

- We need leaders
  - Who wants to take an area?
- Collaboration over Gitlab
  - I can give a git workshop (gentle/30 mins)
    - not today of course!

---slide---

# The Future

- Technical possibilities such as AI assisted agriculture
- Opportunities for engineering and social work students
- Setting a global example


---slide---
# Credits
- Title picture photo credit: Mokhamad Edliadi/CIFOR